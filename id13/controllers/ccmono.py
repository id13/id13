from scipy.optimize import curve_fit
from tabulate import tabulate

from bliss.controllers.monochromator.monochromator import Monochromator


class CCMono(Monochromator):
    """Channel cut monochromator

    - plugin: generic
      package: id13.controllers.ccmono
      class: CCMono
      name: mono
      xtals: $mono_xtals
      motors:
        - bragg_rotation: $ccth
        - energy: $emono
      bragg_scale: -1.004599
      bragg_offset: -0.580355
      undulators:
        - $u18
        - $u35

    - plugin: generic
      package: monochromator
      class: XtalManager
      name: mono_xtals
      xtals:
        - xtal: Si111

    - plugin: generic
      module: monochromator
      class: EnergyCalcMotor
      axes:
       - name: $ccth
         tags: real bragg
       - name: emono
         tags: energy
         unit: keV

    """

    def _xtal_is_in(self, xtal):
        return True

    def _xtal_change(self, xtal):
        raise RuntimeError("Crystal changing is disabled")

    def energy2bragg(self, ene):
        bragg_offset = self.config.get("bragg_offset", 0)
        bragg_scale = self.config.get("bragg_scale", 1)
        return (super().energy2bragg(ene) - bragg_offset) / bragg_scale

    def bragg2energy(self, bragg):
        bragg_offset = self.config.get("bragg_offset", 0)
        bragg_scale = self.config.get("bragg_scale", 1)
        return bragg_scale * super().bragg2energy(bragg + bragg_offset)

    def calibrate(self, angles, energies, bounds=([-3, -0], [-2.8, 2.8])):
        """Calibrate `bragg_scale` and `bragg_offset` from empirical data

        angles: np.array[(deg)]
        energies np.array[(keV)]

        y = np.array([15.199963, 12.999989, 12.499993])
        x = np.array([-6.929140, -8.209840, -8.563440])
        """

        def fit_func(x, kc, ko):
            return kc * self._xtals.bragg2energy(x + ko)

        popt, cov = curve_fit(fit_func, angles, energies)
        table_data = [
            [
                angle,
                fit_func(angle, popt[0], popt[1]),
                energies[i],
                self.bragg2energy(angle),
            ]
            for i, angle in enumerate(angles)
        ]
        print(
            tabulate(
                table_data,
                headers=[
                    "Angle (deg)",
                    "Energy (keV)",
                    "Fitted (keV)",
                    "Current (keV)",
                ],
                floatfmt=".6f",
            )
        )

        print(
            f"""Calculated new cooeficients to be:
    bragg_scale:  {popt[0]:.6f} ({cov[0]})
    bragg_offset: {popt[1]:.6f} ({cov[1]})"""
        )

    def info_calib(self):
        bragg_offset = self.config.get("bragg_offset", 0)
        bragg_scale = self.config.get("bragg_scale", 1)

        return f"""Calibration:
    bragg_scale: {bragg_scale}
    bragg_offset: {bragg_offset}
\n"""

    def info_undu(self):
        info_str = "Undulators:\n"
        for und in self.config.get("undulators", []):
            info_str += f"    {und.name}: {und.position}\n"

        return info_str

    def __info__(self):
        info_str = self._get_info_mono()
        info_str += "\n    "
        info_str += self._get_info_xtals()
        info_str += "\n\n"
        info_str += self._get_info_motors()
        info_str += self.info_calib()
        info_str += self.info_undu()
        return info_str
