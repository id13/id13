# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2019 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
    BLISS controller for VOLPI Intralux DC-1100
"""

import time

from bliss import global_map
from bliss.comm.util import get_comm, SERIAL
from bliss.common.logtools import *
from bliss.controllers.motor import Controller
from bliss.common.axis import AxisState

"""
VOLPI Intralux DC-1100 (DC regulated light source)

YAML configuration example:

- plugin: emotion
  class: volpi
  axes:
    - name: uvolpi
  serial:
    url: tango://id13/serial_132_232/06
    #baudrate: 9600
    #parity: N
    #bytesize: 8
    #stopbits: 1
    #timeout: 5

NOTES:
- this controller has a special protocol of communication where each
  bit is trasmited as a complete byte at 9600 bps with an intercharacter
  spacing of 0.2 ms. this spacing is not allowed by SER2NET.

- the serial tango dserver allows the trasmit byte by byte with the
  requiered spacing without packing them for optimization.

- the serial line configuration parameters (9600N81) are
  not required, they are commented for reference only.
  don't include them.

- the controller only have a receiving channel to set the intensity.
  the read intensity value, is the last instensity set and not
  the intensity read from the controller.

- the intensity is in percent (0% ... 100%) and it is set to 0% at
  initialization.

Example usage:

TURRTEST [10]: uvolpi
     Out [10]: ############################################
               VOLPI Intralux DC-1100
               - DC regulated light source
               - Extremely Stable Light Output: <0.01%
               - Secondary switched DC Regulation
               - Remote Intensity via RS232 Interface
               - 150 Watt Tungsten Halogen Lamp
               Control (uvolpi):
                - intensity: from 0% to 100%
                - tango://id13/serial_132_232/06 (9600N81)
               ############################################

TURRTEST [11]: uvolpi.intensity=40

TURRTEST [12]: uvolpi.intensity
     Out [12]: 39.98044965786901
"""


# --------------  Volpi controller class ------------------


class VolpiMotor(Controller):
    SERIAL_DELAY = 0.02

    # ---------  Initialization/finalization methods -------------

    def __init__(self, name, config, axes, *args, **kwargs):
        """
        Constructor
        Initialization of internal class attributes.
        Called when an object of TURRET controller class is created
        Opens serial line.
        """

        self.configuration = config

        self.__intensity = 0

        for axis_config in axes.values():
            axis_config[1]["steps_per_unit"] = 1
            axis_config[1]["acceleration"] = 1
            axis_config[1]["velocity"] = 1

        try:
            self.serial = get_comm(
                self.configuration,
                SERIAL,
                timeout=5,
                baudrate=9600,
                bytesize=8,
                parity="N",
                stopbits=1,
                eol="\r",
            )
            self._status = f"SERIAL communication configuration found: [{self.serial}]"

        except ValueError:
            try:
                serial_line = self.configuration.get("serial")
                comm_cfg = {"serial": {"url": serial_line}}
                self.serial = get_comm(comm_cfg, timeout=1)
                self._status = (
                    f"'serial_line' keyword is deprecated. Use 'serial' instead\n"
                    + f"SERIAL communication configuration found: [{self.serial}]"
                )
            except:
                self._status = "Cannot find serial configuration"
                raise RuntimeError(f"{self.name}:  [{self._status}]")

        self.serial_conf = (
            f"{self.serial._serial_kwargs['port']} "
            + f"({self.serial._serial_kwargs['baudrate']}"
            + f"{self.serial._serial_kwargs['parity']}"
            + f"{self.serial._serial_kwargs['bytesize']}"
            + f"{self.serial._serial_kwargs['stopbits']})"
        )

        global_map.register(self, children_list=[self.serial])

        time.sleep(self.SERIAL_DELAY * 3)
        log_warning(self, f"{self._status} (Intensity is set to 0)")

        Controller.__init__(self, name, config, axes, *args, **kwargs)

    # ----------------  Information methods ------------------

    def get_id(self, axis):
        """
        Get ID info
        """

        log_info(self, "_get_id()")

        retstr = "VOLPI Intralux DC-1100"
        return retstr

    def get_info(self, axis):
        """
        Get usefull information
        """

        log_info(self, "get_info()")

        _txt = (
            "############################################\n"
            + self.get_id(axis)
            + "\n"
            + "- DC regulated light source\n"
            + "- Extremely Stable Light Output: <0.01%\n"
            + "- Secondary switched DC Regulation\n"
            + "- Remote Intensity via RS232 Interface\n"
            + "- 150 Watt Tungsten Halogen Lamp\n"
            + " - intensity: from 0% to 100%\n"
            + " - "
            + self.serial_conf
            + "\n"
            "############################################\n"
        )

        return _txt

    def __info__(self):
        return self.get_info(None)

    # ----------------  intensity ------------------

    @property
    def intensity(self):
        return self.axes[0].position

    @intensity.setter
    def intensity(self, _val):
        self.axes[0].move(_val)

    # ----------------  motor interface  ----------------

    def initialize_axis(self, axis):
        pass

    def set_velocity(self, axis, new_velocity):
        pass

    def read_velocity(self, axis):
        return 1

    def set_acceleration(self, axis, new_acc):
        pass

    def read_acceleration(self, axis):
        return 1

    def read_position(self, axis):
        return self.__intensity

    def state(self, axis):
        return AxisState("READY")

    def start_one(self, motion):
        val = int((motion.target_pos * 1023.0 / 100.0) + 0.5)
        if (val > 1023) or (val < 0):
            raise RuntimeError(
                f"Invalid value [{motion.target_pos}] (must be from 0 to 100 %)"
            )

        _val_fixed = val / 1023.0 * 100.0

        log_debug(
            self,
            f"volpi intensity: [{motion.target_pos}] fixed: [{_val_fixed}] coded: [{motion.target_pos}]",
        )

        self.__intensity = _val_fixed

        val_comp = 1023 - val
        bit = 1 << 9
        for j in range(10):
            _byte = b"\777" if (val_comp & bit) else b"\000"

            self.serial.write(_byte)
            time.sleep(self.SERIAL_DELAY)
            bit = bit >> 1

        _byte = b"\000"
        for j in range(2):
            self.serial.write(_byte)
            time.sleep(self.SERIAL_DELAY)

    def stop(self, axis):
        pass
