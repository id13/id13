import functools
import sys
import time

from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.scan import Scan
from bliss.scanning.scan_info import ScanInfo
from bliss.scanning.acquisition.ct2 import CT2AcquisitionMaster
from bliss.scanning.scan_progress import ScanProgress

from bliss.controllers.ct2.device import AcqMode as P201AcqMode
from bliss.controllers.lima import lima_base


class SoftwareTimedScan:
    """Run a single trigger software timed scan

    Allows the eiger to frame very quickly (2ms) after recieving a trigger.

    By default is triggered by the P201, but can also be triggered by an
    external user TTL.

    Can optionally elongate the fast shutter signal for the length of the
    acquisition using opiom2
    """

    def _prepare_user_trigger(self):
        """Put shutter in external and use user trigger"""
        self._fsh.mode = self._fsh.EXTERNAL

        self._mux.switch("FS_ENABLE", "ENABLE")
        self._mux.switch("FSH_MODE", "EXT_TRIG")
        self._mux.switch("TRIG_MODE", "EXT_TRIG")

    def _configure_gate(self, nb_frames, expo_time, guard):
        """Configure fast shutter gate
        Use opiom2 to generate a gate signal for fast shutter from trigger
        This will overwrite the Preset_opiom2 configuration

        Connections are :
            uopiom1 o4 -> uopiom2 i5
            uopiom2 o1 -> fastshutter
        """
        freq = 8
        tick = 1 / 8 * 1e6

        ch = 1
        inchannel = 5
        ticks_width = int((expo_time * nb_frames + guard) / tick)
        ticks_delay = 1

        cmd = "CNT %d CLK%d RISE D32 START RISE I%d PULSE %d %d 1" % (
            ch,
            freq,
            inchannel,
            ticks_width,
            ticks_delay,
        )
        print("Eiger gate cmd", cmd)
        self._opiom.comm(cmd)

    def _reset_trigger(self):
        """Put trigger for shutter back"""
        self._fsh.mode = self._fsh.MANUAL

        self._mux.switch("TRIG_MODE", "P201")
        self._mux.switch("FSH_MODE", "CLOSED")

    def ttl_software_scan(
        self,
        nb_frames=15000,
        expo_time=0.0014,
        guard=0.1,
        run=True,
        user_trigger=False,
        gate_shutter=False,
    ):
        """Eiger software scan with single hardware trigger

        Execute a scan with the eiger using a single trigger, and then
        frame internally using software timing. The trigger is P201 by default

        Kwargs:
            user_trigger (bool): Trigger with user TTL (I4 opiom1)
            gate_shutter (bool): Elongate the fast shutter for the length of the acq

        Connections for elongated fast shutter are:
            user ttl -> uopiom1 i4
        """
        chain = AcquisitionChain()
        timer = SoftwareTimerMaster(5, npoints=1)

        detectors = [self._eiger]
        builder = ChainBuilder(detectors)

        lima_params = {
            "acq_mode": "SINGLE",
            "acq_nb_frames": nb_frames,
            "acq_expo_time": expo_time,
            "acq_trigger_mode": "EXTERNAL_TRIGGER",
            "wait_frame_id": range(nb_frames),
            "prepare_once": True,
            "start_once": True,
        }

        if not gate_shutter:
            p201_acq_params = {
                "npoints": 1,
                "acq_expo_time": 5,
                "acq_mode": P201AcqMode.IntTrigMulti,
            }

            p201_acq_obj = CT2AcquisitionMaster(self._p201, **p201_acq_params)
            chain.add(timer, p201_acq_obj)
            master = p201_acq_obj
        else:
            master = timer

        for node in builder.get_nodes_by_controller_type(lima_base.Lima):
            node.set_parameters(acq_params=lima_params)
            chain.add(master, node)

        print(chain._tree)
        builder.print_tree(not_ready_only=False)

        command_line = f"eiger_software(nb_frame: {nb_frames}, expo: {expo_time}, user_trigger={user_trigger}, gate={gate_shutter})"
        scan_info = ScanInfo()
        scan_info.update(
            {
                "title": command_line,
                "npoints": nb_frames,
                "count_time": expo_time,
                "type": "eiger_software",
            }
        )

        sc = Scan(
            chain,
            name=command_line,
            scan_info=scan_info,
            scan_progress=SoftwareScanDisplay(self._eiger, nb_frames),
        )

        def do_run():
            try:
                if user_trigger:
                    self._prepare_user_trigger()
                    if gate_shutter:
                        self._configure_gate(nb_frames, expo_time, guard)
                sc.run()
            finally:
                # Close shutter
                self._fsh.close()

                # Put shutter trigger back
                if user_trigger:
                    self._reset_trigger()

                # Put eiger trigger mode back
                self._eiger.acq_trig_mode = "EXTERNAL_TRIGGER_MULTI"

        if run:
            do_run()
        else:
            sc.do_run = do_run

        return sc

    @functools.wraps(ttl_software_scan)
    def ttl_software_scan_hws(self, *args, **kwargs):
        with self.hws_manager():
            self.ttl_software_scan(*args, **kwargs)

    def simulate_trigger(self):
        """Trigger User Input

        Simulate external user ttl trigger

        Connections:
            opiom2 o8 -> opiom1 in4
        """
        self._opiom.comm("IMA 0x80")
        val = self._opiom.comm("?IMA")
        assert val == "0x80"

        time.sleep(0.01)

        self._opiom.comm("IMA 0x0")
        val = self._opiom.comm("?IMA")
        assert val == "0x00"


class SoftwareScanDisplay(ScanProgress):
    def __init__(self, eiger, nb_frames):
        super().__init__()
        self._eiger = eiger
        self._nb_frames = nb_frames

    def build_display_line(self):
        det_name = self._eiger.name
        last_image = self.data.get(f"{det_name}:last_image_ready", -1)
        return f"det[{det_name}] img[{last_image+1}/{self._nb_frames}]"

    def progress_callback(self):
        print(self.build_display_line() + "\r", end="")
        sys.stdout.flush()

    def scan_end_callback(self):
        print(self.build_display_line())
