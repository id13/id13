from .hardware_saving import EigerHardwareSaving
from .software_scan import SoftwareTimedScan


class Eiger(EigerHardwareSaving, SoftwareTimedScan):
    """Wrapper around the eiger camera to provide extra functionality

     * Add hardware saving
     * Add software timed scans with external trigger

    ```yaml
        plugin: bliss
        package: id13.controllers.eiger
        class: Eiger
        name: neiger
        config:
            eiger: $eiger
            shutter: $nfsh
            multiplexer: $nmux
            opiom: $uopiom2
    ```
    """

    def __init__(self, name, config_tree):
        config = config_tree["config"]

        self._eiger = config.get("eiger")
        self._mux = config.get("multiplexer")
        self._fsh = config.get("shutter")
        self._opiom = config.get("opiom")
        self._p201 = config.get("p201")
