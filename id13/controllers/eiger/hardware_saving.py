from contextlib import contextmanager
from bliss import current_session


class EigerHardwareSaving:
    def hws(self, state):
        """Enable / disable eiger hardware saving (FileWriter)"""
        if state:
            self._eiger.saving._managed_mode = "HARDWARE"
            self._eiger.saving.file_format = "HDF5"
            current_session.scan_saving.images_prefix = (
                "{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}"
            )
        else:
            self._eiger.saving._managed_mode = "SOFTWARE"
            self._eiger.saving.file_format = "HDF5BS"
            current_session.scan_saving.images_prefix = (
                "{img_acq_device}/{collection_name}_{dataset_name}_{scan_number}_data_"
            )

    @contextmanager
    def hws_manager(self):
        self.hws(True)
        try:
            yield
        finally:
            self.hws(False)
