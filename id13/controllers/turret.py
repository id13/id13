# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2019 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
    BLISS controller for OLYMPUS microscope objetive turret
"""

import time

from bliss import global_map
from bliss.comm.util import get_comm, SERIAL
from bliss.common.logtools import *
from bliss.config.beacon_object import BeaconObject


"""
OLYMPUS microscope objetive turret controller

YAML configuration example:

- name: uturret
  plugin: bliss
  class: turret
  serial:
    url: ser2net://lid132:28000/dev/ttyR6
    #baudrate: 19200
    #parity: N
    #bytesize: 8
    #timeout: 5
    #stopbits: 1

NOTE: the serial line configuration parameters (19200N81) are
      not requiered, they are commented for reference only.
      don't include them.
       
Example usage:

TURRTEST [1]: nturret
     Out [1]: ##################################################
              OLYMPUS microscope objetive turret controller
               - model: BX-REMCB
              Controller (nturret):
               - position values: 1 ... 5
               - ser2net://lid133:28000/dev/ttyR38 (19200N81)
              ##################################################

TURRTEST [2]: nturret.mode
     Out [2]: 'remote'

TURRTEST [4]: nturret.mode = "local"
TURRTEST [5]: nturret.mode
     Out [5]: 'local'

TURRTEST [7]: nturret.position =  5
TURRTEST [8]: nturret.position
     Out [8]: 5

TURRTEST [9]: nturret.mode
     Out [9]: 'remote'

NOTE: when the position is read or set, the mode is changed to "remote"
      if it is in "local"

"""

# --------------  Volpi controller class ------------------


class turret(BeaconObject):
    MODES = {"remote": "IN", "local": "OUT"}

    # ---------  Initialization/finalization methods -------------

    def __init__(self, *args, **kwargs):
        """
        Constructor
        Initialization of internal class attributes.
        Called when an object of TURRET controller class is created
        Opens serial line.
        """

        self.name = args[0]
        self.configuration = args[1]
        self.__mode = "unknown"

        self.__position = 0

        super().__init__(self.configuration)

        try:
            self.serial = get_comm(
                self.configuration,
                SERIAL,
                timeout=5,
                baudrate=19200,
                bytesize=8,
                parity="N",
                stopbits=1,
                eol="\r",
            )
            self._status = f"SERIAL communication configuration found: [{self.serial}]"

        except ValueError:
            try:
                serial_line = self.configuration.get("serial")
                comm_cfg = {"serial": {"url": serial_line}}
                self.serial = get_comm(comm_cfg, timeout=1)
                self._status = (
                    f"'serial_line' keyword is deprecated. Use 'serial' instead\n"
                    + f"SERIAL communication configuration found: [{self.serial}]"
                )
            except:
                self._status = "Cannot find serial configuration"
                raise RuntimeError(f"{self.name}:  [{self._status}]")

        self.serial_conf = (
            f"{self.serial._serial_kwargs['port']} "
            + f"({self.serial._serial_kwargs['baudrate']}"
            + f"{self.serial._serial_kwargs['parity']}"
            + f"{self.serial._serial_kwargs['bytesize']}"
            + f"{self.serial._serial_kwargs['stopbits']})"
        )

        global_map.register(self, children_list=[self.serial])

        self.mode = "remote"

        log_info(self, f"{self._status}")

    # ----------------  comm ------------------
    def putget_cmd_check(self, cmd):
        ans = self.putget_cmd(cmd)

        cmd_s = cmd.split()
        ans_s = ans.split()

        if (len(ans_s) == 2) and (cmd_s[0] == ans_s[0]) and (ans_s[1] == "+"):
            return 0
        else:
            log_error(self, f"CMD ERROR: [{cmd}] -> [{ans}]")
            return 1

    def putget_cmd_query(self, cmd):
        _cmd = cmd + "?"
        ans = self.putget_cmd(_cmd)

        ans_s = ans.split()

        if (len(ans_s) != 2) or (cmd != ans_s[0]):
            log_error(self, f"CMD ERROR: [{cmd}] -> [{ans}]")
            return "error"

        return ans_s[1]

    def putget_cmd(self, cmd):
        _cmd = cmd + "\r"
        self.serial.write(_cmd.encode())
        time.sleep(0.2)
        return self.serial.readline().decode().rstrip()

    # ----------------  Information methods ------------------

    def get_id(self):
        """
        Get ID info
        """

        log_info(self, "_get_id()")

        retstr = "OLYMPUS microscope objetive turret controller"
        return retstr

    def get_info(self):
        """
        Get usefull information
        """

        log_info(self, "get_info()")

        _txt = (
            "##################################################\n"
            + self.get_id()
            + "\n"
            + " - model: BX-REMCB\n"
            + "Controller ("
            + self.name
            + "):\n"
            + " - position values: 1 ... 5\n"
            + " - "
            + self.serial_conf
            + "\n"
            "##################################################\n"
        )

        return _txt

    def __info__(self):
        return self.get_info()

    # ----------------  mode ------------------

    def _set_mode(self, val):
        if self.__mode == val:
            return

        if not (val in self.MODES):
            raise RuntimeError(f"{self.name}: Invalid mode [{val}]")

        mode = self.MODES[val]

        cmd = "1LOG " + mode
        if self.putget_cmd_check(cmd):
            raise RuntimeError(f"{self.name}: FAILED set mode")

        cmd = "1LOG"
        ans = self.putget_cmd_query(cmd)
        if ans != mode:
            raise RuntimeError(f"{self.name}: Invalid controller response")

        self.__mode = val

    @property
    def mode(self):
        return self.__mode

    @mode.setter
    def mode(self, val):
        self._set_mode(val)

    # ----------------  position ------------------

    @BeaconObject.property(default=True)
    def position(self):
        self._set_mode("remote")

        cmd = "1OB"
        ans = self.putget_cmd_query(cmd)
        if ans == "error":
            raise RuntimeError(f"{self.name}: Invalid controller response")

        pos = int(ans)
        log_debug(self, f"{self.name} position: [{pos}]")

        self.__position = pos
        return pos

    @position.setter
    def position(self, _val):
        self._set_mode("remote")

        val = int(_val)
        if (val > 5) or (val < 1):
            raise RuntimeError(
                f"{self.name}: Invalid value: position [{_val}] must be 1 ... 5)"
            )

        log_debug(self, f"{self.name} new position: [{_val}]")

        cmd = "1OB %d" % (val,)
        if self.putget_cmd_check(cmd):
            raise RuntimeError(f"{self.name}: Invalid controller response]")

        self.__position = val
