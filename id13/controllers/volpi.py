# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2019 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
    BLISS controller for VOLPI Intralux DC-1100
"""

import time

from bliss import global_map
from bliss.comm.util import get_comm, SERIAL
from bliss.common.logtools import *
from bliss.config.beacon_object import BeaconObject


"""
VOLPI Intralux DC-1100 (DC regulated light source)

YAML configuration example:

- name: uvolpi
  plugin: bliss
  class: volpi
  serial:
    url: tango://id13/serial_132_232/06
    #baudrate: 9600
    #parity: N
    #bytesize: 8
    #stopbits: 1
    #timeout: 5

NOTES:
- this controller has a special protocol of communication where each
  bit is trasmited as a complete byte at 9600 bps with an intercharacter
  spacing of 0.2 ms. this spacing is not allowed by SER2NET.
  
- the serial tango dserver allows the trasmit byte by byte with the 
  requiered spacing without packing them for optimization. 
  
- the serial line configuration parameters (9600N81) are
  not requiered, they are commented for reference only.
  don't include them.
       
- the controller only have a receiving chanlel to set the intensity.
  the read intensity value, is the last instensity set and not
  the intensity read from the controller.
  
- the intensity is in percent (0% ... 100%) and it is set to 0% at
  initialization. 
  
Example usage:

TURRTEST [10]: uvolpi
     Out [10]: ############################################
               VOLPI Intralux DC-1100
               - DC regulated light source
               - Extremely Stable Light Output: <0.01%
               - Secondary switched DC Regulation
               - Remote Intensity via RS232 Interface
               - 150 Watt Tungsten Halogen Lamp
               Control (uvolpi):
                - intensity: from 0% to 100%
                - tango://id13/serial_132_232/06 (9600N81)
               ############################################

TURRTEST [11]: uvolpi.intensity=40

TURRTEST [12]: uvolpi.intensity 
     Out [12]: 39.98044965786901
"""


# --------------  Volpi controller class ------------------


class volpi(BeaconObject):
    SERIAL_DELAY = 0.02

    # ---------  Initialization/finalization methods -------------

    def __init__(self, *args, **kwargs):
        """
        Constructor
        Initialization of internal class attributes.
        Called when an object of TURRET controller class is created
        Opens serial line.
        """

        self.name = args[0]
        self.configuration = args[1]

        self.__intensity = 0

        super().__init__(self.configuration)

        try:
            self.serial = get_comm(
                self.configuration,
                SERIAL,
                timeout=5,
                baudrate=9600,
                bytesize=8,
                parity="N",
                stopbits=1,
                eol="\r",
            )
            self._status = f"SERIAL communication configuration found: [{self.serial}]"

        except ValueError:
            try:
                serial_line = self.configuration.get("serial")
                comm_cfg = {"serial": {"url": serial_line}}
                self.serial = get_comm(comm_cfg, timeout=1)
                self._status = (
                    f"'serial_line' keyword is deprecated. Use 'serial' instead\n"
                    + f"SERIAL communication configuration found: [{self.serial}]"
                )
            except:
                self._status = "Cannot find serial configuration"
                raise RuntimeError(f"{self.name}:  [{self._status}]")

        self.serial_conf = (
            f"{self.serial._serial_kwargs['port']} "
            + f"({self.serial._serial_kwargs['baudrate']}"
            + f"{self.serial._serial_kwargs['parity']}"
            + f"{self.serial._serial_kwargs['bytesize']}"
            + f"{self.serial._serial_kwargs['stopbits']})"
        )

        global_map.register(self, children_list=[self.serial])

        time.sleep(self.SERIAL_DELAY * 3)
        self.intensity = 0
        log_warning(self, f"{self._status} (Intensity is set to 0)")

    # ----------------  Information methods ------------------

    def get_id(self):
        """
        Get ID info
        """

        log_info(self, "_get_id()")

        retstr = "VOLPI Intralux DC-1100"
        return retstr

    def get_info(self):
        """
        Get usefull information
        """

        log_info(self, "get_info()")

        _txt = (
            "############################################\n"
            + self.get_id()
            + "\n"
            + "- DC regulated light source\n"
            + "- Extremely Stable Light Output: <0.01%\n"
            + "- Secondary switched DC Regulation\n"
            + "- Remote Intensity via RS232 Interface\n"
            + "- 150 Watt Tungsten Halogen Lamp\n"
            + "Control ("
            + self.name
            + "):\n"
            + " - intensity: from 0% to 100%\n"
            + " - "
            + self.serial_conf
            + "\n"
            "############################################\n"
        )

        return _txt

    def __info__(self):
        return self.get_info()

    # ----------------  intensity ------------------
    @BeaconObject.property(default=True)
    def intensity(self):
        return self.__intensity

    @intensity.setter
    def intensity(self, _val):
        val = int((_val * 1023.0 / 100.0) + 0.5)
        if (val > 1023) or (val < 0):
            raise RuntimeError(f"Invalid value [{_val}] (must be from 0 to 100 %)")

        _val_fixed = val / 1023.0 * 100.0

        log_debug(
            self, f"volpi intensity: [{_val}] fixed: [{_val_fixed}] coded: [{val}]"
        )

        self.__intensity = _val_fixed

        val_comp = 1023 - val
        bit = 1 << 9
        for j in range(10):
            _byte = b"\777" if (val_comp & bit) else b"\000"

            self.serial.write(_byte)
            time.sleep(self.SERIAL_DELAY)
            bit = bit >> 1

        _byte = b"\000"
        for j in range(2):
            self.serial.write(_byte)
            time.sleep(self.SERIAL_DELAY)
