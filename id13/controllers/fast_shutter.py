SHUTTER_STATE = {True: "OPEN", False: "CLOSED"}


class FastShutter:
    """Wrapper for fast shutter to set mode and state simultaneously

    ```yaml
        plugin: bliss
        package: id13.fast_shutter
        class: FastShutter
        name: nfshw
        config:
          shutter: $nfsh
          multiplexer: $nmux

          # optional
          motor: $ntubez
          in_pos: 1
          out_pos: 0

          # mux trigger options
          mux_triggers:
            SHUTTER: P201
          mux_states:
            SHUTTER: SHUTTER
            DETECTOR: DETECTOR
    ```
    """

    def __init__(self, name, config_tree):
        config = config_tree.get("config")
        self._fsh = config["shutter"]
        self._mux = config["multiplexer"]

        self._motor = config.get("motor")
        self._in_pos = config.get("in_pos")
        self._out_pos = config.get("out_pos")

        self._mux_states = config.get("mux_states", [])
        self._mux_triggers = config.get("mux_triggers", [])

    def _get_shutter_inout(self):
        if not self._motor:
            return "No motor configured"

        zpos = self._motor.position
        x = round(zpos * 10)
        if x == self._in_pos:
            return "SHUTTER IN"
        elif x == self._out_pos:
            return "SHUTTER_OUT"
        else:
            return "SHUTTER Z NOT ALIGNED"

    def trigger(self):
        self._fsh.mode = self._fsh.EXTERNAL
        for key, value in self._mux_triggers.items():
            self._mux.switch(key, value)

    def close(self):
        self._fsh.mode = self._fsh.MANUAL
        self._fsh.close()

    def open(self):
        self._fsh.mode = self._fsh.MANUAL
        self._fsh.open()

    def move_out(self):
        self._motor.move(self._out_pos / 10)

    def move_in(self):
        self._motor.move(self._in_pos / 10)

    def __info__(self):
        info_str = "Shutter info:\n"
        if self._motor:
            info_str += "    shutter in/out:\t\t%s\n" % self._get_shutter_inout()
        info_str += "    shutter state (%s):\t%s\n" % (
            self._fsh.name,
            SHUTTER_STATE[self._fsh.is_open],
        )
        info_str += "    shutter mode (%s):\t%s\n" % (self._fsh.name, self._fsh.mode)
        for name, key in self._mux_states.items():
            info_str += "    %s %s:\t\t%s\n" % (
                self._mux.name,
                name,
                self._mux.getOutputStat(key),
            )

        return info_str

    def __enter__(self):
        self.trigger()
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.close()
