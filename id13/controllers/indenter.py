import gevent
from tango import DeviceProxy


class Indenter:
    def __init__(self, name, config):
        self.config = config
        self.name = name
        self._encoder = DeviceProxy(config["encoder"])
        self._force = DeviceProxy(config["force"])
        self._piezo = DeviceProxy(config["piezo"])
        self.indenter = DeviceProxy(config["indenter"])

    # def fork_test(self,nb):
    #    def priint_loop():
    #        for i in range(nb):
    #            print(f"test {i}/{nb}")
    #            gevent.sleep(1)
    #
    #    self.task = gevent.spawn(priint_loop)
