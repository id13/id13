from bliss.controllers.expression_based_calc import (
    ExpressionCalcCounter,
    SingleExpressionCalcCounterController,
)
import numexpr


class ExpTimeNormaliserCounterController(SingleExpressionCalcCounterController):
    def calc_function(self, input_dict):
        exp_dict = self._constants.to_dict()
        for cnt in self.inputs:
            exp_dict.update({self.tags[cnt.name]: input_dict[self.tags[cnt.name]]})

        exp_dict["count_time"] = self._count_time
        return {
            self.tags[self.outputs[0].name]: numexpr.evaluate(
                self._expression, global_dict={}, local_dict=exp_dict
            )
        }

    def get_acquisition_object(self, acq_params, ctrl_params, parent_acq_params, acq_devices):
        count_time = acq_params.get("count_time")
        if count_time:
            self._count_time = acq_params.get("count_time")
        return super().get_acquisition_object(acq_params, ctrl_params, parent_acq_params, acq_devices)

    def get_default_chain_parameters(self, scan_params, acq_params):
        self._count_time = scan_params["count_time"]
        return super().get_default_chain_parameters(scan_params, acq_params)


class ExpTimeNormaliserCounter(ExpressionCalcCounter):
    """Expression based calc counter exposing `count_time`

    ```yaml
        plugin: bliss
        package: id13.controllers.bliss.exp_time_counter
        class: ExpTimeNormaliserCounter
        name: exp_time_norm
        expression: (m*x+b)/count_time
        inputs:
          - counter : $diode
            tags: x
        constants:
            m: 10
            b: 1
    ```

    """

    def apply_config(self, reload=False):
        self.constants.apply_config(reload)
        if reload:
            self._config.reload()

        self._unit = self._config.get("unit")
        name = self._config["name"]
        calc_ctrl_config = {
            "inputs": self._config["inputs"],
            "outputs": [{"name": name, "tags": name}],
        }
        self._set_controller(
            ExpTimeNormaliserCounterController(
                name + "_ctrl",
                calc_ctrl_config,
                self._config["expression"],
                self.constants,
            )
        )
