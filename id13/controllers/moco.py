# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2015-2019 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
    BLISS controller for MoCo Monochromator Controller
"""

from bliss import global_map
from bliss.comm.util import get_comm, SERIAL
from bliss.common.logtools import *

"""
MOCO Isg Monochromator Controller

YAML configuration example:

- name: umoco
  plugin: bliss
  class: moco
  serial:
    url: tango://id13/serial_132_232/06
    #baudrate: 9600
    #parity: N
    #bytesize: 8
    #stopbits: 1
    #timeout: 5

NOTES:
- the serial line configuration parameters (9600N81) are
  not requiered, they are commented for reference only.
  don't include them.
       
  
Configuration:
    INBEAM [{CURR | VOLT | EXT}] [{NORM | INV}] [{BIP | UNIP}] [<fS>] [{AUTO | NOAUTO}]
                                                   |--- BIP return error (no option?)
    
    RH [18]: umoco.cmd_put_ack("INBEAM CURR NORM UNIP 5e-05 NOAUTO")                                                                   
    Out [18]: 'OK'

    RH [19]: umoco.cmd_query("INBEAM")                                                                                                 
    Out [19]: 'CURR NORM UNIP 5e-05 NOAUTO'


    OUTBEAM  [{CURR  |  VOLT  |  EXT}]  [{NORM  |  INV}]  [{BIP |  UNIP}]  [<fS>]  [{AUTO  | NOAUTO}] 
    
    
    RH [21]: umoco.cmd_put_ack("OUTBEAM VOLT INV BIP 0.01 AUTO")                                                                       
    Out [21]: 'OK'

    RH [22]: umoco.cmd_query("OUTBEAM")                                                                                                
    Out [22]: 'VOLT INV BIP 1.25 AUTO'
 
    
                                                       
Example usage:

TURRTEST [1]: umoco
     Out [1]: ##################################################
              MOCO 02.00  -  Current settings:
                NAME "ID13 Monochromator"
                ADDR MC1

                OPRANGE -10 10 0
                INBEAM CURR NORM UNIP 5e-05 NOAUTO
                GAIN INBEAM DEFAULT
                OUTBEAM CURR NORM UNIP 5e-05 NOAUTO
                GAIN OUTBEAM DEFAULT

                MODE POSITION
                SLOPE 7.52316e-37
                SETPOINT 0
                TAU 0.01
                SET  NORMALISE
                CLEAR  AUTORUN BEAMCHECK AUTORANGE INTERLOCK
                AUTOTUNE OFF BEAMLOSS
                AUTOPEAK OFF
                BEAMCHECK 0 0.2 2.048 0
                SRANGE -10 10
                SPEED 2 10
                INHIBIT OFF LOW

              Controller (umoco):
               - ser2net://lid132:28000/dev/ttyR3 (9600N81)
              ##################################################

TURRTEST [2]: umoco.outgain
     Out [2]: '5e-05'

TURRTEST [3]: umoco.outgain=7.8e-7

TURRTEST [4]: umoco.outgain
     Out [4]: '1e-06'

TURRTEST [5]: umoco.ingain
     Out [5]: '5e-05'

TURRTEST [6]: umoco.ingain=3.1e-8

TURRTEST [7]: umoco.ingain
     Out [7]: '5e-08'

TURRTEST [8]: umoco.inbeam
     Out [8]: '0'

TURRTEST [9]: umoco.outbeam
     Out [9]: '0'

TURRTEST [13]: umoco.outgain=7.8e-9

TURRTEST [14]: umoco.outgain
     Out [14]: '1e-08'

TURRTEST [15]: umoco.outbeam
     Out [15]: '9.01149e-12'

      RH [18]: umoco.cmd_put_ack("INBEAM CURR NORM UNIP 5e-05 NOAUTO")                                                                   
     Out [18]: 'OK'

      RH [19]: umoco.cmd_query("INBEAM")                                                                                                     
     Out [19]: 'CURR NORM UNIP 5e-05 NOAUTO'
    
      RH [21]: umoco.cmd_put_ack("OUTBEAM VOLT INV BIP 0.01 AUTO")                                                                       
     Out [21]: 'OK'

      RH [22]: umoco.cmd_query("OUTBEAM")                                                                                                
     Out [22]: 'VOLT INV BIP 1.25 AUTO'

"""


# --------------  MOCO controller class ------------------


class moco(object):
    SERIAL_DELAY = 0.02

    # ---------  Initialization/finalization methods -------------

    def __init__(self, *args, **kwargs):
        """
        Constructor
        Initialization of internal class attributes.
        Called when an object of TURRET controller class is created
        Opens serial line.
        """

        self.name = args[0]
        self.config = args[1]

        # self.__intensity = 0

        try:
            self.serial = get_comm(
                self.config,
                SERIAL,
                timeout=5,
                baudrate=9600,
                bytesize=8,
                parity="N",
                stopbits=1,
                eol="\n",
            )
            self._status = f"SERIAL communication configuration found: [{self.serial}]"

        except ValueError:
            try:
                serial_line = self.config.get("serial")
                comm_cfg = {"serial": {"url": serial_line}}
                self.serial = get_comm(comm_cfg, timeout=1)
                self._status = (
                    f"'serial_line' keyword is deprecated. Use 'serial' instead\n"
                    + f"SERIAL communication configuration found: [{self.serial}]"
                )
            except:
                self._status = "Cannot find serial configuration"
                raise RuntimeError(f"{self.name}:  [{self._status}]")

        self.serial_conf = (
            f"{self.serial._serial_kwargs['port']} "
            + f"({self.serial._serial_kwargs['baudrate']}"
            + f"{self.serial._serial_kwargs['parity']}"
            + f"{self.serial._serial_kwargs['bytesize']}"
            + f"{self.serial._serial_kwargs['stopbits']})"
        )

        global_map.register(self, children_list=[self.serial])

        # self.intensity = 0

        log_debug(self, f"{self._status} (Intensity is set to 0)")

    # ----------------  Information methods ------------------

    def get_id(self):
        """
        Get ID info
        """

        log_info(self, "_get_id()")

        retstr = "MOCO Isg Monochromator Controller"
        return retstr

    def get_info(self):
        """
        Get usefull information
        """
        sep = ("#" * 50) + "\n"
        _txt = (
            sep
            + self.cmd_query("INFO")
            + "\n\n"
            + "Controller ("
            + self.name
            + "):\n"
            + " - "
            + self.serial_conf
            + "\n"
            + sep
        )

        return _txt

    def __info__(self):
        return self.get_info()

    # ----------------  comm ------------------

    def cmd_put_ack(self, _cmd):
        cmd = "#" + _cmd
        self.serial.write((cmd + "\r").encode())

        ans = self.serial.readline().decode().strip()

        msg = f"[{cmd}] ---> [{ans}]"
        log_debug(self, msg)

        if ans != "OK":
            raise RuntimeError(f"ERROR Invalid ACK {msg}")

        return ans

    def cmd_query(self, _cmd):
        cmd = "?" + _cmd
        self.serial.write((cmd + "\r").encode())

        ans = self.serial.readline()
        if ans.startswith("$".encode()):
            ans = self.serial.readline("$\r\n".encode())
        ans = ans.decode().strip()

        msg = f"[{cmd}] ---> [{ans}]"
        log_debug(self, msg)

        return ans

    # ----------------  beam intensity ------------------

    @property
    def inbeam(self):
        cmd = "beam"
        ans = self.cmd_query(cmd)
        return ans.split()[0]

    @property
    def outbeam(self):
        cmd = "beam"
        ans = self.cmd_query(cmd)
        return ans.split()[1]

    # ----------------  gain ------------------
    @property
    def outgain(self):
        cmd = "outbeam"
        ans = self.cmd_query(cmd)
        return ans.split()[3]

    @outgain.setter
    def outgain(self, _val):
        val = 1.0 * _val
        cmd = "outbeam %g" % (val,)
        self.cmd_put_ack(cmd)

    @property
    def ingain(self):
        cmd = "inbeam"
        ans = self.cmd_query(cmd)
        return ans.split()[3]

    @ingain.setter
    def ingain(self, _val):
        val = 1.0 * _val
        cmd = "inbeam %g" % (val,)

        self.cmd_put_ack(cmd)
