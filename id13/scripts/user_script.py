import os.path

from bliss import current_session
from bliss.config.settings import SimpleSetting

USER_SCRIPT_HOME = SimpleSetting(
    "%s:script_home" % current_session.name,
    default_value="%s/bliss_scripts" % os.getenv("HOME"),
)

__all__ = ["user_script_directory", "user_script_run", "user_script_load"]


def user_script_directory(new_dir=None):
    if new_dir is not None:
        if not os.path.isdir(new_dir):
            raise RuntimeError(f"Invalid directory [{new_dir}]")
        USER_SCRIPT_HOME.set(new_dir)
    else:
        return USER_SCRIPT_HOME.get()


def user_script_load(scriptname):
    _user_script_exec(scriptname, export=True)


def user_script_run(scriptname):
    _user_script_exec(scriptname, export=False)


def _user_script_exec(scriptname, export=False):
    script_dir = os.path.abspath(USER_SCRIPT_HOME.get())
    (filename, fileext) = os.path.splitext(scriptname)
    if not fileext:
        fileext = ".py"
    filepath = os.path.join(script_dir, "".join((filename, fileext)))
    if not os.path.isfile(filepath):
        raise RuntimeError(f"Cannot find [{filepath}] !!")
    try:
        script = open(filepath).read()
    except:
        raise RuntimeError(f"Failed to read [{filepath}] !!")
    print(f"Running [{filepath}] ...")
    globals_dict = current_session.env_dict.copy()
    exec(script, globals_dict)

    if export is True:
        for k in globals_dict.keys():
            if k.startswith("_"):
                continue
            current_session.env_dict[k] = globals_dict[k]
