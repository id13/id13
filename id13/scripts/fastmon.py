import sys
import numpy
import time

from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave
from bliss.scanning.acquisition.musst import MusstAcquisitionSlave
from bliss.scanning.acquisition.musst import MusstAcquisitionMaster

from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan
from bliss.scanning.scan import is_zerod
from bliss import setup_globals
from contextlib import contextmanager
from .cscantools import *


# --------------------------------------------------------------------
# compute(self, sender, in_data_dict, index):
# """
# Fill 'output_data' dictionnary with OUTPUT DATA.
# * <in_data_dict> contains INPUT DATA delivered block by block.
# * <in_data_dict> contains only 1 key (data of a channel) by call.
# """


# input_key = list(in_data_dict.keys())[0]
# if input_key in self.input_names_list:
# input_data = in_data_dict[input_key]
# else:
## print(f"pass on IK {input_key}")
# return
# --------------------------------------------------------------------


class musstPreset(ScanPreset):
    def __init__(self, musst_device):
        self.musst_device = musst_device
        # self.mux = session.get_current_session().config.get('multiplexer_tomo')

    def prepare(self, scan):
        self.musst_device.putget("CHCFG CH1 CNT")
        self.musst_device.putget("TMRCFG 10MHZ")
        self.musst_device.putget("HSIZE 0 0")
        self.musst_device.putget("ESIZE 524288 1")
        emem, ebuf = self.musst_device.putget("?ESIZE").split(" ")
        nr_values = int(int(emem) / 2)
        ch1 = self.musst_device.putget("?CHCFG CH1")
        tmr = self.musst_device.putget("?TMRCFG")

        print(
            f"... musstPreset ch1[{ch1}] tmr[{tmr}] nr_values[{nr_values}] nr_buf[{ebuf}]"
        )


# class PcoTomoShutterPreset(ScanPreset):

# def __init__(self, soft_shutter):

# self.soft_shutter = soft_shutter
# self.shtime = None
# self.mux = session.get_current_session().config.get('multiplexer_tomo')

# def prepare(self,scan):
# motor_master = scan.acq_chain.nodes_list[0]
# scan_info = scan.scan_info

# if scan_info['soft_shut_time'] > 0:
# accpos = motor_master._calculate_undershoot(scan_info['start_pos'])
# disp = scan_info['start_pos'] - accpos
# mottime = abs(disp / scan_info['speed'])
# self.shtime = mottime - scan_info['soft_shut_time']

# def start(self, scan):
# scan_info=scan.scan_info
# if scan_info['soft_shut_time'] > 0:
# self.soft_shutter.open()
# if self.shtime <= 0:
# time.sleep(-self.shtime)

# if scan_info['sync_shut_time'] > 0:
# self.mux.switch("SHMODE","MUSST")

# def stop(self, scan):
# scan_info=scan.scan_info
# if scan_info['sync_shut_time'] > 0:
# self.mux.switch("SHMODE","SOFT")

# if scan_info['soft_shut_time'] > 0:
# self.soft_shutter.close()


# p = MyPreset()
# scan.add_preset(p)
# ================================================================
# ================================================================
def func_calc(sender, datain):
    key = list(datain.keys())[0]
    if key == "TIMER":
        value = []
        value0 = datain[key]
        l = len(value0)
        for i in range(l):
            val = value0[i]
            if val < 0:
                val = val + 4294967296
            value.append(val / 1.0e7)
        # 2138535799
        # value =  value0 / 1.0e7
        # print(f"... func_calc: sender [{sender}] dict[{datain}] key [{key}] in[{value0}] out[{value}]")
        return {"time": value}

    # if(key == "CH1"):
    # value0 = datain[key]
    # value =  value0 +5.0
    ##print(f"... func_calc: sender [{sender}] dict[{datain}] key [{key}] in[{value0}] out[{value}]")
    # return {"count": value}

    # print(f"... ERRORfunc_calc: sender [{sender}] dict[{datain}] key [{key}]")
    return datain


# ================================================================
# ================================================================
def fastmon(
    nb_points,
    sampling_time,
):
    """
    Usage:
    fastmon
       <nr point> <sampling time (s)>
    """
    # measurement = setup_globals.ACTIVE_MG
    # enabled_counters_name = measurement.enabled

    # create the acquisition chain
    chain = AcquisitionChain(parallel_prepare=True)

    musst_device = setup_globals.umusst1

    store_list = ["TIMER", "CH1"]  # channels read from musst

    timer_factor = musst_device.get_timer_factor()
    stime = int(numpy.ceil(sampling_time * timer_factor))

    print(
        f"... points[{nb_points}] timer_factor[{timer_factor}] sampling_time[{sampling_time}] stime[{stime}]"
    )

    # MUSST master
    musst_master = MusstAcquisitionMaster(
        musst_device,
        program="fastmon.mprg",
        program_start_name="FASTMON",
        program_abort_name="CLEANUP",
        # program_template_replacement=template_replacement,
        vars={
            "NPULSES": nb_points,
            "SAMPLING_TIME": stime,
        },
    )

    # MUSST acquisition slave
    musst_slave = MusstAcquisitionSlave(
        musst_device, store_list=store_list
    )  # store encoder and timer data

    calc_timer = CalcChannelAcquisitionSlave(
        "calc_timer",
        (musst_slave,),
        func_calc,
        # ["time", "count"],          # channels calculated
        [
            "time",
        ],  # channels calculated
    )

    chain.add(musst_master, musst_slave)
    chain.add(musst_master, calc_timer)

    preset = musstPreset(musst_device)

    scan_info = {
        "title": f"fastmon points[{nb_points}], sampling[{sampling_time}]",
        "type": "",
    }

    # display = KmapCScanDisplay(detectors_2d, mcas, nb_points)
    # scan = Scan(chain, name="fastmon", scan_info=scan_info, data_watch_callback=display)
    scan = Scan(chain, name="fastmon", scan_info=scan_info)
    scan.add_preset(preset)
    scan.run()
    # multiplexer = setup_globals.multiplexer_eh3
    # with _multiplexer(multiplexer, musst_device):
    # try:
    # scan.run()
    # finally:
    # display.print_last_display()
    ## delete eiger file for now
    # if "eiger" in enabled_counters_name:
    # eiger_cam = setup_globals.eiger
    # e = eiger_cam._get_proxy("Eiger")
    # e.deletememoryfiles()
    return scan


# ================================================================
# ================================================================
class KmapCScanDisplay(object):
    def __init__(self, detectors, mcas, nb_points, channel_name=None):
        self.nb_points = nb_points
        self.detectors = detectors
        self.mcas = mcas
        self.detector_info = dict()
        self.channel_name = channel_name
        self.last_display = ""

    #    def on_state(self,state):
    #        return True

    def print_last_display(self):
        print((self.last_display))

    def __call__(self, data_events, nodes, info):
        display_list = list()
        for det in self.detectors:
            curr_image = det.proxy.last_image_ready
            display_list.append(
                "{0} {1}/{2}".format(det.name, curr_image + 1, self.nb_points)
            )
        for mca in self.mcas:
            curr_point = mca._proxy.get_current_pixel()
            display_list.append(
                "{0} {1}/{2}".format(mca.name, curr_point, self.nb_points)
            )

        for acq_device, events in list(data_events.items()):
            data_node = nodes.get(acq_device)
            if is_zerod(data_node):
                if self.channel_name == data_node.name:
                    point_number = len(data_node)
                    display_list.insert(
                        0, "point %d/%d" % (point_number, self.nb_points)
                    )
        display = " ".join(display_list)
        if display:
            print(display, "\r", end=" ")
            # print(display,'\r', end=' ')
            sys.stdout.flush()
        self.last_display = display


@contextmanager
def _multiplexer(multiplexer, musst_device):
    started = time.time()
    musst_device.putget("IO !IO9")  # Force shutter to be closed
    multiplexer.switch("DET1_POL", "NORMAL")  # XIA negative logic on O2
    multiplexer.switch("DET2_POL", "INVERTED")  # Eiger positive on O3
    multiplexer.switch("DET3_POL", "INVERTED")
    multiplexer.switch("DET4_POL", "INVERTED")
    multiplexer.switch("DETECTOR", "MUSST_BTRIG")
    multiplexer.switch("shutter", "MUSST_GATE_BACK", synchronous=True)
    try:
        yield
    finally:
        multiplexer.switch("DET1_POL", "NORMAL")
        multiplexer.switch("DET2_POL", "NORMAL")
        multiplexer.switch("DET3_POL", "NORMAL")
        multiplexer.switch("DET4_POL", "NORMAL")
        multiplexer.switch("DETECTOR", "P201")
        multiplexer.switch("SHUTTER", "P201")
        musst_device.putget("IO !IO9")  # Force shutter to be closed
        print(("Took: %s seconds" % (time.time() - started)))
