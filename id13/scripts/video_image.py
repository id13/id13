import numpy
import tango

try:
    from blissdata.lima.image_utils import decode_devencoded_video
except ModuleNotFoundError:
    from blissdata.data.lima_image import decode_devencoded_video


def video_image(camera: str, grayscale: bool = False) -> numpy.ndarray:
    lima_proxy = tango.DeviceProxy(f"id13/limaccds/{camera}")
    raw_video_image = lima_proxy.video_last_image
    image_data = decode_devencoded_video(raw_video_image)
    if not image_data:
        return

    image_data = image_data[0]

    if grayscale:
        image_data = numpy.dot(image_data[..., :3], [0.2989, 0.5870, 0.1140]).astype(
            int
        )

    return image_data
