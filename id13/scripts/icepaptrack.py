from bliss.controllers.motors.icepap.comm import _command
from bliss import current_session


def enable(axis):
    address = axis.config.config_dict["address"]
    _command(axis.controller._cnx, f"{address}:TRACK ENCIN")


def disable(axis):
    address = axis.config.config_dict["address"]
    _command(axis.controller._cnx, f"{address}:STOP")
    axis.sync_hard()


def resolution(axis, steps=32):
    assert steps in [8, 16, 32, 64, 128]
    address = axis.config.config_dict["address"]
    _command(axis.controller._cnx, f"{address}:_cfg einnstep {steps}")
    enable(axis)


def stage_enable():
    for motor in ["microx", "microy", "microz"]:
        obj = current_session.env_dict[motor]
        enable(obj)


def stage_disable():
    for motor in ["microx", "microy", "microz"]:
        obj = current_session.env_dict[motor]
        disable(obj)


def stage_resolution(x_res: int = 32, y_res: int = 32, z_res: int = 32):
    resolution(current_session.env_dict["microx"], x_res)
    resolution(current_session.env_dict["microy"], y_res)
    resolution(current_session.env_dict["microz"], z_res)


class IcePapTrack:
    def __init__(self, name, config_tree):
        self.name = name
        self._current_resolution = None

    def enable(self):
        stage_enable()

    def disable(self):
        stage_disable()

    @property
    def resolution(self) -> int:
        return self._current_resolution

    @resolution.setter
    def resolution(self, resolution: int):
        self._current_resolution = resolution
        stage_resolution(resolution, resolution, resolution)
