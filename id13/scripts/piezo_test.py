from bliss.common import cleanup
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan
from bliss.scanning.acquisition.motor import MotorMaster
from bliss.scanning.acquisition.pi import PIAcquisitionDevice


def calibrate(motor, distance, time, nb_points=1000):
    with cleanup.cleanup(motor, restore_list=(cleanup.axis.POS,)):
        master = MotorMaster(
            motor,
            motor.position(),
            motor.position() + distance,
            npoints=nb_points,
            time=time,
        )
        total_time = time + 0.2
        sampling_time = float(total_time) / nb_points

        recorder = PIAcquisitionDevice(
            motor.controller, nb_points, sampling_time, trigger_source=motor.MOTION
        )
        recorder.set_counters(
            "%s_pos" % motor.name,
            motor,
            motor.CURRENT_POSITION_OF_AXIS,
            "%s_error" % motor.name,
            motor,
            motor.POSITION_ERROR_OF_AXIS,
        )
        master.add_external_channel(recorder, "timestamp")  # ,rename='time')

        chain = AcquisitionChain(parallel_prepare=True)
        chain.add(master, recorder)

        scan = Scan(
            chain,
            name="calibrate",
            scan_info={
                "title": "PI calibration of %s with movement %f"
                % (motor.name, distance)
            },
            writer=None,
        )
        scan.run()
        print("End Scan")
