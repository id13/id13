from bliss import setup_globals

def wago_light(intensity: int):
    wcid13g = setup_globals.wcid13g

    for i in range(4):
        wcid13g.set(f"ao{i+1}", intensity)
        wcid13g.set(f"do{i+1}", 0 if intensity == 0 else 1)
