from typing import List

from bliss.scanning.scan import ScanPreset


class MultiplexerScanPreset(ScanPreset):
    def __init__(self, multiplexer, values: List[List[str]]):
        self._multiplexer = multiplexer
        self._values = values

    def prepare(self, scan):
        pass

    def start(self, chain):
        for switch, start_value, _ in self._values:
            self._multiplexer.switch(switch, start_value)

    def stop(self, chain):
        for switch, _, stop_value in self._values:
            self._multiplexer.switch(switch, stop_value)
