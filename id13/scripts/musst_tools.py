# ===================================================
# 2021/02/19 - from id15 MPapillon
# ===================================================


def check_max_timer(musst_obj, scan_time):
    if get_max_timer(musst_obj) < scan_time:
        print("... MUSST must clock will be FIXED")
        return set_max_timer(musst_obj, scan_time)
    print("... MUSST must clock is OK, not change")
    return False


def set_max_timer(musst_obj, total_time):
    (clock_str, clock) = musst_obj.TMRCFG
    clock_list = [1e3, 10e3, 100e3, 1e6, 10e6, 50e6]
    clock_cmds = ["1KHZ", "10KHZ", "100KHZ", "1MHZ", "10MHZ", "50MHZ"]
    clock_index = clock_list.index(clock)
    changed = False

    while clock_index >= 0 and total_time > (pow(2, 32) / clock_list[clock_index]):
        clock_index -= 1
        changed = True

    if changed:
        musst_obj.TMRCFG = clock_cmds[clock_index]
        musst_obj.__last_clock = clock_cmds[clock_index]
        # self.channels.get("timer").resolution = clock_list[clock_index]
        print(
            f"... MUSST now [{clock_str} {clock}] is changed to [{clock_cmds[clock_index]} {clock_list[clock_index]}]"
        )
    else:
        print(f"... MUSST now [{clock_str}] [{clock}] NO change")

    max_scan_time = int(get_max_timer(musst_obj))
    print(
        f"... MUSST max_scan_time [{max_scan_time} sec] [{int(max_scan_time/60)} min]"
    )

    return changed


def get_max_timer(musst_obj):
    return pow(2, 32) / musst_obj.get_timer_factor()
