import time
from bliss import setup_globals


def restore_live(video_exposure=0.001):
    dlvlm1 = setup_globals.dlvlm1
    dlvlm1.proxy.acq_trigger_mode = "INTERNAL_TRIGGER"
    dlvlm1.proxy.video_source = "BASE_IMAGE"
    dlvlm1.proxy.acq_expo_time = video_exposure
    dlvlm1.proxy.acq_nb_frames = 1

    print("running single acq")
    dlvlm1.proxy.prepareAcq()
    dlvlm1.proxy.startAcq()
    dlvlm1.proxy.stopAcq()
    time.sleep(1)

    print("enabling video")
    dlvlm1.proxy.video_exposure = video_exposure
    dlvlm1.proxy.video_live = True


def stop_live():
    dlvlm1 = setup_globals.dlvlm1
    dlvlm1.proxy.video_live = False
