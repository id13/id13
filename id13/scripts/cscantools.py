__all__ = [
    "CScanMusstChanTrigCalc",
    "CScanMusstChanStepCalc",
    "_get_musst_template",
    "_get_musst_store_list",
    "_step_per_unit",
]

import numpy
import sys
from bliss.scanning.chain import AcquisitionChannel
from bliss.scanning.scan import Scan

from bliss.common.utils import grouped

_step_per_unit = lambda mot: (
    mot.encoder.steps_per_unit if mot.encoder else mot.steps_per_unit
)


class CScanMusstChanTrigCalc(object):
    """Musst Trig Data Channel

    trigger up value
    trigger down value
    mean trigger value ((up+down)/2)
    delta position (down - up)
    """

    def __init__(self, source_name, factor, dest_name):
        self._source_name = source_name
        self._factor = factor
        self._dest_name = dest_name

        self._data = numpy.zeros((0), dtype=numpy.int32)

    def prepare(self):
        self._data = numpy.zeros((0), dtype=numpy.int32)

    def __call__(self, sender, data_dict):
        data = data_dict.get(self._source_name)
        if data is None:
            return {}

        self._data = numpy.append(self._data, data)

        up_data = list()
        down_data = list()
        mean_data = list()
        delta_data = list()

        for up, down in grouped(self._data, 2):
            up /= self._factor
            down /= self._factor
            up_data.append(up)
            down_data.append(down)
            mean_data.append((up + down) / 2.0)
            delta_data.append(down - up)

        if not len(up_data):
            return {}

        self._data = self._data[2 * len(up_data) :]

        return {
            "%s_%s" % (self._dest_name, x): numpy.array(data, dtype=float)
            for x, data in (
                ("mean", mean_data),
                ("up", up_data),
                ("down", down_data),
                ("delta", delta_data),
            )
        }

    @property
    def acquisition_channels(self):
        return [
            AcquisitionChannel("%s_%s" % (self._dest_name, x), float, ())
            for x in ["up", "down", "mean", "delta"]
        ]


class CScanMusstChanStepCalc(object):
    """Musst Step Data Channel

    channel value between 2 consecutive up triggers
    """

    def __init__(self, source_name, factor, dest_name):
        self._source_name = source_name
        self._factor = factor
        self._dest_name = dest_name

        self._data = numpy.zeros((0), dtype=numpy.int32)

    def __call__(self, sender, data_dict):
        data = data_dict.get(self._source_name)
        if data is None:
            return {}

        self._data = numpy.append(self._data, data)

        step_data = list()
        last_up = self._data[0] / self._factor
        for _, up in grouped(self._data[1:], 2):
            up /= self._factor
            step_data.append(up - last_up)
            last_up = up

        if not len(step_data):
            return {}

        self._data = self._data[2 * len(step_data) :]

        return {"%s_step" % self._dest_name: numpy.array(step_data, dtype=float)}

    @property
    def acquisition_channels(self):
        return [AcquisitionChannel("%s_step" % self._dest_name, float, ())]


def _get_musst_template(master_motor, *other_chans):
    """Return musst replacement template"""
    (master_name, master_chan) = master_motor

    data_alias = []
    data_store = []
    for name, chan in other_chans:
        data_alias.append("ALIAS DATA%d = CH%d\n" % (chan, chan))
        data_store.append("DATA%d " % (chan))

    template_replacement = {
        "$MOTOR_CHANNEL$": "CH%d" % master_chan,
        "$DATA_ALIAS$": "\n".join(data_alias),
        "$DATA_STORE$": " ".join(data_store),
    }
    return template_replacement


def _get_musst_store_list(master_motor, *other_chans):
    """Return list of channel name ordered by channel index"""
    chan_list = [master_motor] + list(other_chans)

    def _chan_sort(x):
        return x[1]

    chan_list.sort(key=_chan_sort)
    return ["timer"] + [name for (name, _) in chan_list]
